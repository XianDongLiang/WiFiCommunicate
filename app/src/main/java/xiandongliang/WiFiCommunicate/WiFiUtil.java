package xiandongliang.WiFiCommunicate;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class WiFiUtil {

    private String TAG = "WiFiUtil";
    private static WiFiUtil INSTANCE;
    private WifiManager wifiManager;
    private WifiManager.WifiLock wifiLock;

    private WiFiUtil(Context context){

        wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }

    public static WiFiUtil getInstance(Context context){

        if (INSTANCE == null){

            INSTANCE = new WiFiUtil(context);
        }

        return INSTANCE;
    }

    /*打开Wi-Fi*/
    public void openWiFi(){

        if (!wifiManager.isWifiEnabled()){
            wifiManager.setWifiEnabled(true);
        }

    }

    /*关闭Wi-Fi*/
    public void closeWiFi(){

        if (wifiManager.isWifiEnabled()){
            wifiManager.setWifiEnabled(false);
        }

    }

    /*检查当前Wi-Fi网卡状态*/
    private void checkWiFiState(){

        int state = wifiManager.getWifiState();

        if (state == WifiManager.WIFI_STATE_DISABLING){
            Log.d(TAG,"WIFI_STATE_DISABLED->Wi-Fi网卡不可用");
        }else if (state == WifiManager.WIFI_STATE_DISABLED){
            Log.d(TAG,"WIFI_STATE_DISABLED->Wi-Fi网卡正在关闭");
        }else if (state == WifiManager.WIFI_STATE_ENABLING){
            Log.d(TAG,"WIFI_STATE_ENABLING->Wi-Fi网卡正在打开");
        }else if (state == WifiManager.WIFI_STATE_ENABLED){
            Log.d(TAG,"WIFI_STATE_ENABLED->Wi-Fi网卡可用");
        }else if (state == WifiManager.WIFI_STATE_UNKNOWN){
            Log.d(TAG,"WIFI_STATE_UNKNOWN->Wi-Fi网卡状态未知");
        }

    }

    /*开始扫描设备周围Wi-Fi热点*/
    private void startScan(){

        if (wifiManager.startScan()){
            Log.d(TAG,"扫描设备周围Wi-Fi热点成功");
        }else{
            Log.d(TAG,"扫描设备周围Wi-Fi热点失败");
        }
    }

    /*获取设备周围Wi-Fi热点信息*/
    private List<ScanResult> getWiFiList(){
        Log.d(TAG,"获取设备周围Wi-Fi热点信息");
        return  wifiManager.getScanResults();
    }

    /*获取当前连接Wi-Fi热点信息*/
    private WifiInfo getConnectWiFiInfo(){
        Log.d(TAG,"获取当前连接Wi-Fi热点信息");
        return wifiManager.getConnectionInfo();
    }

    /*获取设备中已保存Wi-Fi热点信息*/
    private List<WifiConfiguration> getWiFiConfiguration(){
        Log.d(TAG,"获取设备中已保存Wi-Fi热点信息");
        return wifiManager.getConfiguredNetworks();
    }

    /*断开当前连接Wi-Fi热点*/
    private boolean disconnectWiFi(){
        Log.d(TAG,"断开当前连接Wi-Fi热点");
        return wifiManager.disconnect();
    }

    /*连接到指定Wi-Fi热点*/
    private boolean connectDefaultWiFi(int defaultNetWorkID){
        Log.d(TAG,"连接到指定Wi-Fi热点");
        return wifiManager.enableNetwork(defaultNetWorkID,true);
    }

    /*创建Wi-Fi锁*/
    private void createWiFiLock(){
        Log.d(TAG,"创建Wi-Fi锁");
        wifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF,"SCM-CONNECTION");
    }

    /*锁定当前连接Wi-Fi热点*/
    private void lockConnectWiFi(){
        Log.d(TAG,"锁定当前Wi-Fi热点");
        wifiLock.acquire();
    }

    /*解锁当前连接Wi-Fi热点*/
    private void unlockConnectWiFi(){
        Log.d(TAG,"解锁当前连接Wi-Fi热点");
        if (wifiLock.isHeld()){
            wifiLock.release();
        }
    }

    /*设备开启Wi-Fi热点*/
    public void createdWiFiHotspots(String SSID,String PASSWORD,int PORT){

        closeWiFi();

        /**设置WiFiConfiguration参数*/
        WifiConfiguration wifiConfiguration = new WifiConfiguration();
        /**Wi-Fi热点名称*/
        wifiConfiguration.SSID = SSID;
        /**Wi-Fi热点连接密码*/
        wifiConfiguration.preSharedKey = PASSWORD;
        /**Wi-Fi热点是否隐藏*/
        wifiConfiguration.hiddenSSID = false ;
        /**Wi-Fi热点配置是否启用*/
        wifiConfiguration.status = WifiConfiguration.Status.ENABLED;
        /**IEEE 802.11认证算法 OPEN*/
        wifiConfiguration.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
        /**秘钥管理WPA_PSK*/
        wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        /**组秘钥TKIP+CCMP*/
        wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        /**对称秘钥TKIP+CCMP*/
        wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);

        try {
                Method Enable_Method = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, Boolean.TYPE);
                boolean status = (Boolean) Enable_Method.invoke(wifiManager, wifiConfiguration, true);

                if (status) {
                    Log.d(TAG,"SSID："+SSID);
                    Log.d(TAG,"PASSWORD："+PASSWORD);
                } else {
                    Log.d(TAG,"设备热点创建失败");
                }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG,"设备热点创建失败");
        }

    }

    /*设备关闭Wi-Fi热点*/
    public void closeWiFiHotspots(){

        try {
                Method AP_Method = wifiManager.getClass().getMethod("getWifiApConfiguration");
                AP_Method.setAccessible(true);
                WifiConfiguration wifiConfiguration = (WifiConfiguration) AP_Method.invoke(wifiManager);
                Method Enable_Method = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
                Enable_Method.invoke(wifiManager, wifiConfiguration, false);

        } catch (NoSuchMethodException e) {
                e.printStackTrace();
        } catch (IllegalArgumentException e) {
                e.printStackTrace();
        } catch (IllegalAccessException e) {
                e.printStackTrace();
        } catch (InvocationTargetException e) {
                e.printStackTrace();
        }

    }

    /*判断当前连接Wi-Fi热点是否为默认的Wi-Fi热点*/
    public void discriminationConnectWiFi(int defaultNetWorkID){

        openWiFi();

        WifiInfo wifiInfo = getConnectWiFiInfo();

        /*当前连接的NetWorkID*/
        int NetWorkID = wifiInfo.getNetworkId();

        /*当前Wi-Fi热点是否为指定Wi-Fi热点
         * true->对当前Wi-Fi热点进行锁定
         * false->对当前Wi-Fi热点进行断开，重新连接到指定Wi-Fi热点并且锁定
         * */
        if (defaultNetWorkID == NetWorkID){
            Log.d(TAG,"当前Wi-Fi是默认Wi-Fi");
            createWiFiLock();
            lockConnectWiFi();
        }else if (disconnectWiFi()){
            Log.d(TAG,"断开当前Wi-Fi连接成功");
            if (connectDefaultWiFi(defaultNetWorkID)){
                Log.d(TAG,"连接到指定Wi-Fi成功");
                createWiFiLock();
                lockConnectWiFi();
            }else {
                Log.d(TAG,"连接到指定Wi-Fi失败");
            }

            }else {
                Log.d(TAG,"断开当前Wi-Fi连接失败");
            }

    }

    /*Logcat设备中所有保留的Wi-Fi热点信息*/
    public void logoutAllWifiConfiguration(){

        final List<WifiConfiguration> wifiConfigurationList = getWiFiConfiguration();
        final int SIZE = wifiConfigurationList.size();

        WifiConfiguration item;

        for (int i=0 ; i<SIZE ; i++){

              item = wifiConfigurationList.get(i);

              if(item.status == 0){
                  Log.d(TAG,"Statue：null");
              }else {
                  Log.d(TAG,"Statue："+String.valueOf(item.status));
              }

              if (item.networkId == 0){
                  Log.d(TAG,"NetWorkID：null");
              }else {
                  Log.d(TAG,"NetWorkID："+String.valueOf(item.networkId));
              }

              if(item.SSID == null){
                  Log.d(TAG,"SSID：null");
              }else {
                  Log.d(TAG,"SSID："+item.SSID);
              }

              if (item.hiddenSSID){
                  Log.d(TAG,"The Wi-Fi has been hidden");
              }else {
                  Log.d(TAG,"The Wi-Fi has not been hidden");
              }

              if (item.BSSID == null) {
                  Log.d(TAG, "BSSID：null" );
              }else {
                  Log.d(TAG,"BSSID："+item.BSSID);
              }

        }
    }

}
