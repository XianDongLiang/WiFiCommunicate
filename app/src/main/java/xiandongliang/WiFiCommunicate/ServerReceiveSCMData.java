package xiandongliang.WiFiCommunicate;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerReceiveSCMData implements Runnable{

    private ServerSocket serverSocket;
    private Socket socket;

    public ServerReceiveSCMData(int PORT){

        try{

            serverSocket = new ServerSocket(PORT);

        }catch (IOException e){

            e.printStackTrace();
        }

    }

    @Override
    public void run(){

        /*temp存储读取一个字节*/
        byte[] temp = new byte[1];

        try {
            Log.d("socket","正在等待客户端的连接");
            socket = serverSocket.accept();
            Log.d("socket","连接成功");
        }catch (IOException e){
            e.printStackTrace();
        }

        /*一般情况下是每获取一个socket的入站连接，打开一个输入流，但Android服务端程序与单片机连接为一一对应关系，所以Android只接受一个客户端的入站连接*/
        while (true){

            try {

                /*获取客户端的输入流*/
                InputStream inputStream = socket.getInputStream();

                /*size用于检测inputStream中的是否有可读取的数据流*/
                int size = inputStream.available();

                if (size != 0){

                    /*read(byte[] b[])方法会返回一个整型*/
                    inputStream.read(temp);
                    String HEXString = bytesToHexString(temp);

                    /*打印输出获取的数据*/
                    Log.d("HEXString",HEXString);
                }

            }catch (IOException e){
                e.printStackTrace();
            }catch (NullPointerException e){
                e.printStackTrace();
            }

        }

    }

    /**将输入的byte数组转换为16进制的字符串*/
    private String bytesToHexString(byte[] bytes){

        String HEXString = "";

        for (int i = 0;i < bytes.length; i++) {

            String hex = Integer.toHexString(bytes[i] & 0xFF);

            if (hex.length() == 1) {

                hex = '0' + hex;

            }

            HEXString += hex.toUpperCase();
        }

        return HEXString;
    }

}
