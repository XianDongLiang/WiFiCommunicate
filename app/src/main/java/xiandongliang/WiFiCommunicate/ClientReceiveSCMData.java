package xiandongliang.WiFiCommunicate;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;

public class ClientReceiveSCMData implements Runnable{

    private OutputStream outputStream;
    private InputStream inputStream;
    private String IP;
    private int PORT;


    public ClientReceiveSCMData(String IP, int PORT){

        this.IP = IP;
        this.PORT = PORT;

    }


    @Override
    public void run(){

        try{

            Socket socket = new Socket(IP,PORT);
            outputStream = socket.getOutputStream();
            inputStream = socket.getInputStream();

        }catch (IOException e){
            e.printStackTrace();
        }

        while (true){

            byte[] temp = new byte[1024];

            try{

                if (inputStream.available() != 0){

                    Log.d("SIZE",String.valueOf(inputStream.available()));
                    inputStream.read(temp);
                    String HEXString = bytesToHexString(temp);

                    Log.d("HEXString",HEXString);
                }

            }catch (IOException e){
                e.printStackTrace();
            }

        }

    }

    private void HeartBeat(final byte[] heartBeat){


        if (heartBeat == null){

            Log.d("HeartBeat","NULL");
        }

        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {

                try {

                    outputStream.write(heartBeat);
                    outputStream.write('\n');
                    outputStream.flush();

                }catch (IOException e){
                    e.printStackTrace();
                }

                Log.d("Timer","000");

            }
        };

        /*每隔0.5秒请求一次数据*/
        timer.schedule(timerTask,0,500);
    }

    /**将输入的byte数组转换为16进制的字符串*/
    private String bytesToHexString(byte[] bytes){

        String HEXString = "";

        for (int i = 0;i < bytes.length; i++) {

            String hex = Integer.toHexString(bytes[i] & 0xFF);

            if (hex.length() == 1) {

                hex = '0' + hex;

            }

            HEXString += hex.toUpperCase();
        }

        return HEXString;
    }
    /*开辟两条线程，一条线程是用于每隔x秒向TCP服务端请求一次数据，一条线程是一直开启监听数据的输入*/

}
