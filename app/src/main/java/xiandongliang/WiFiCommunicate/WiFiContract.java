package xiandongliang.WiFiCommunicate;

import java.net.ServerSocket;

public interface WiFiContract {

    void getLocalIPAddress(ServerSocket socket);
    void ClientReceiveSCMData();
    void ServerReceiveSCMData();
}
