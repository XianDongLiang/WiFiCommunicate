package xiandongliang.WiFiCommunicate;

import android.util.Log;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.SocketException;
import java.util.Enumeration;

public class ReceiveData implements WiFiContract{

    @Override
    public void ClientReceiveSCMData(){

    }

    @Override
    public void ServerReceiveSCMData(){

    }

    @Override
    public void getLocalIPAddress(ServerSocket serverSocket){

        try {

            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();){
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();){
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    String mIP = inetAddress.getHostAddress().substring(0, 3);
                    if(mIP.equals("192")){
                        String IP = inetAddress.getHostAddress();    //获取本地IP
                        int PORT = serverSocket.getLocalPort();    //获取本地的PORT
                        Log.e("IP",""+IP);
                        Log.e("PORT",""+PORT);
                    }
                }
            }

        } catch (SocketException e) {
            e.printStackTrace();
        }

    }

    /**将输入的byte数组转换为16进制的字符串*/
    private String bytesToHexString(byte[] bytes){

        String HEXString = "";

        for (int i = 0;i < bytes.length; i++) {

            String hex = Integer.toHexString(bytes[i] & 0xFF);

            if (hex.length() == 1) {

                hex = '0' + hex;

            }

            HEXString += hex.toUpperCase();
        }

        return HEXString;
    }

}
